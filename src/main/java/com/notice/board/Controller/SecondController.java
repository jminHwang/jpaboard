package com.notice.board.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SecondController {

    @GetMapping("/noticeTest")
    public String helloString(){

        return "공지사항 입니다.";
    }
}
