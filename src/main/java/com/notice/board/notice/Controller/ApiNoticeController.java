package com.notice.board.notice.Controller;

import com.notice.board.notice.Entity.Notice;
import com.notice.board.notice.Model.NoticeInput;
import com.notice.board.notice.Model.NoticeModel;
import com.notice.board.notice.Repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api")
public class ApiNoticeController {

    private final NoticeRepository noticeRepository;
    /*
    @GetMapping("/notice")
    public String noticeString(){
        return "공지사항 입니다.";
    }
     */
/*    @GetMapping("/notice")
    public NoticeModel notice(){
        LocalDateTime regDate = LocalDateTime.of(2021,2,8, 0, 0);

        NoticeModel notice = new NoticeModel();
        notice.setId(1);
        notice.setTitle("공지사항입니다.");
        notice.setContents("공지사항 내용입니다.");
        notice.setRegDate(regDate);

        return notice;
    }*/

    @GetMapping("/notice")
    public List<NoticeModel> notice(){

        List<NoticeModel> noticeList = new ArrayList<>();

        LocalDateTime regDate = LocalDateTime.of(2021,2,8, 0, 0);

        NoticeModel notice1 = new NoticeModel();
        notice1.setId(1);
        notice1.setTitle("공지사항입니다.");
        notice1.setContents("공지사항 내용입니다.");
        notice1.setRegDate(regDate);

        NoticeModel notice2 = new NoticeModel();
        notice2.setId(2);
        notice2.setTitle("공지사항 2 입니다.");
        notice2.setContents("공지사항 내용4 입니다.");
        notice2.setRegDate(regDate);

        noticeList.add(notice1);
        noticeList.add(notice2);

        return noticeList;
    }

/*    @PostMapping("/notice")
    public NoticeModel addNotice(@RequestParam String title, @RequestParam String contents){
        NoticeModel notice = NoticeModel.builder().id(1).title(title).contents(contents).regDate(LocalDateTime.now()).build();
        return notice;
    }*/

    @PostMapping("/notice")
    public Notice addNotice(@RequestBody NoticeInput noticeInput){

        Notice notice = Notice.builder().title(noticeInput.getTitle()).contents(noticeInput.getContents()).regDate(LocalDateTime.now()).build();

        noticeRepository.save(notice);

        return notice;
    }


}
